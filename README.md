Nama    : Mhd. Fattahilah Rangkuty | 
NRP     : 05111850010015 | 
Kelas   : TD Desain Audit Jaringan |

Judul Paper : A Modified AODV Routing Protocol to Avoid Black Hole Attack in MANETs

Tarek M. Mahmoud, Abdelmgeid A. Aly, Omar Makram M. 
Faculty of Science, Computer Science Department, Minia University, Minia Egypt



Modifikasi protokol AODV untuk menghindari serangan balckhole attack, protokol yang diajukan MIASAODV

1. Melakukan modifikasi pada RREP_WAIT_TIME, yaitu menggandakan waktu tunggu untuk menerima RREP lebih banyak, agar dapat memilih node yang berbahaya dengan metode
DSN yang paling tinggi dan arrival time tercepat.
